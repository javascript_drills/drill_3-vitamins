# Drill on Array of objects in JavaScript

## Drill contains question on Various Operations on Objects in the Array.

### Following are the Questions in the drill

1. Filtering out the items having availability status as true.

2. Filtering out Items containing only Vitamin C.

3. Filtering out Items containing Vitamin A.

4. Grouping items according to Vitamins.

5. Sorting items according to number of contents.