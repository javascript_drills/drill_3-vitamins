const itemData = require(`./3-arrays-vitamins.cjs`);

const VitaminAItems = itemData.filter(({contains}) => contains.includes("Vitamin A"));

console.log(VitaminAItems);