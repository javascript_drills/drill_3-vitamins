const itemData = require(`./3-arrays-vitamins.cjs`);

const VitaminsGroup = {} ;

for(let index = 0 ; index < itemData.length ; index++ ){

   let names = itemData[index]["name"] ;

    let contains = itemData[index]["contains"].split(',') ;

    for(let arrayIndex = 0 ; arrayIndex < contains.length ; arrayIndex++ ){
     

        let vitamin = contains[arrayIndex].trim() ;

        if(VitaminsGroup[vitamin] === undefined){

            VitaminsGroup[vitamin] = [] ;

            VitaminsGroup[vitamin].push(names);
        }else if (VitaminsGroup[vitamin][names] === undefined){

            VitaminsGroup[vitamin].push(names);
        }
    }

}

console.log(VitaminsGroup);