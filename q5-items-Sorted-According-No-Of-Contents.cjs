const itemData = require(`./3-arrays-vitamins.cjs`);

const object = {};

for (let index = 0; index < itemData.length; index++) {

    let vitamins = itemData[index]["contains"].split(",");

    let item = itemData[index]["name"];

    object[item] = vitamins.length;

}

let sortedArray = [];

for (let key in object) {

    sortedArray.push([key, object[key]]);

}

sortedArray.sort(function (a, b) {

    return b[1] - a[1];

});

console.log(sortedArray);